#!/bin/bash

CONT_NAME=nginx-network

set -ex 

docker kill $CONT_NAME | true
docker rm $CONT_NAME | true

docker ps

docker run --name $CONT_NAME \
    -d \
    -p 80:80 \
    -p 443:443 \
    --restart always \
    --net mynetwork \
    nginx-network

docker logs $CONT_NAME