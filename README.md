# Nginx_OIDC

1. nginx;
2. openSSL;
3. lua-nginx-module;
4. ngx-devel-kit;
5. lua-5.4.4;
6. lua-resty-core;
7. lua-resty-lrucache;

# Getting started

Before run container create and push your website cert to dir *certs/* and edit file *conf/nginx.conf*.
After that modify in nginx.conf *example.com* to *your_site_name.domain*.

# Author
Exception003

# License
GPLv3
