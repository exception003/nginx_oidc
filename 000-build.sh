#! /bin/bash


if [[ -z "${FLAG_SOURCE_ENV}" ]]; then
  echo "environment variables are not set! please use 'source 000-...'"
  exit 1
fi

CONT_NAME=nginx-network
docker build -t $CONT_NAME .
