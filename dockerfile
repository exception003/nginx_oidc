FROM debian:stable

ENV LUA_LIB=/usr/local/lib/lua/5.4/
ENV LUA_INC=/usr/local/include/
ENV LUAJIT_LIB=/usr/local/lib
ENV LUAJIT_INC=/tmp/nginx-lua/luajit2/src
ENV LUA_VERSION=5.4

USER root

RUN apt -y update && apt -y install git wget luarocks build-essential

RUN mkdir /tmp/nginx-lua

WORKDIR /tmp/nginx-lua

RUN wget http://nginx.org/download/nginx-1.22.0.tar.gz \
    && wget https://github.com/openssl/openssl/archive/OpenSSL_1_1_1o.tar.gz \
    && wget https://github.com/openresty/lua-nginx-module/archive/v0.10.21.tar.gz \
    && wget https://github.com/simplresty/ngx_devel_kit/archive/v0.3.1.tar.gz \
    && wget http://www.lua.org/ftp/lua-5.4.4.tar.gz \
    && wget https://github.com/openresty/lua-resty-core/archive/refs/tags/v0.1.23.tar.gz \
    && wget https://github.com/openresty/lua-resty-lrucache/archive/refs/tags/v0.11.tar.gz

RUN tar -zxvf nginx-1.22.0.tar.gz \
    && tar -zxvf OpenSSL_1_1_1o.tar.gz \
    && tar -zxvf v0.10.21.tar.gz \
    && tar -zxvf v0.3.1.tar.gz \
    && tar -zxvf lua-5.4.4.tar.gz \
    && tar -zxvf v0.1.23.tar.gz \
    && tar -zxvf v0.11.tar.gz

RUN git clone https://github.com/openresty/luajit2 \
    && git clone https://github.com/facebookarchive/luaffifb

WORKDIR /tmp/nginx-lua/luajit2

RUN make && make install

WORKDIR /tmp/nginx-lua/luaffifb

RUN luarocks make

WORKDIR /tmp/nginx-lua/lua-5.4.4

RUN make linux test
RUN make install

WORKDIR /tmp/nginx-lua/lua-resty-core-0.1.23

RUN make && make install

WORKDIR /tmp/nginx-lua/lua-resty-lrucache-0.11

RUN make && make install

WORKDIR /tmp/nginx-lua

RUN luarocks install lua-resty-http \
    && luarocks install lua-resty-session \
    && luarocks install lua-resty-jwt 0.2.0 \
    && luarocks install lua-cjson \
    && luarocks install lua-resty-openidc

RUN apt install -y libreadline-dev libpcre2-posix2 libpcre3-dev zlib1g zlib1g-dev

WORKDIR /tmp/nginx-lua/nginx-1.22.0

RUN ./configure --prefix=/opt/nginx --with-http_ssl_module --with-ld-opt="-Wl,-rpath,/usr/local/lib/lua/5.4/" --add-module=/tmp/nginx-lua/ngx_devel_kit-0.3.1 --add-module=/tmp/nginx-lua/lua-nginx-module-0.10.21 --with-openssl=/tmp/nginx-lua/openssl-OpenSSL_1_1_1o

RUN make -j2
RUN make install

RUN mkdir /opt/nginx/ssl

COPY /certs/* /opt/nginx/ssl/
COPY /conf/nginx.conf /opt/nginx/conf/nginx.conf
COPY /conf/proxy.conf /opt/nginx/conf/proxy.conf

WORKDIR /opt/nginx/sbin/

EXPOSE 80
EXPOSE 443

RUN ./nginx -t

CMD ["./nginx", "-g", "daemon off;"]